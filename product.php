<?php include('header.php'); ?>
		<!-- BREADCRUMBS -->
		<section class="breadcrumb parallax margbot30"></section>
		<!-- //BREADCRUMBS -->
		
		
		<!-- TOVAR DETAILS -->
		<section class="tovar_details padbot70">
			
			<!-- CONTAINER -->
			<div class="container">
				
				<!-- ROW -->
				<div class="row">
					
					<!-- SIDEBAR TOVAR DETAILS -->
					<div class="col-lg-3 col-md-3 sidebar_tovar_details">
						<h3><b>other sweaters</b></h3>
						
						<ul class="tovar_items_small clearfix">
							<li class="clearfix">
								<img class="tovar_item_small_img" src="images/tovar/women/1.jpg" alt="" />
								<a href="product-page.html" class="tovar_item_small_title">Embroidered bib peasant top</a>
								<span class="tovar_item_small_price">$88.00</span>
							</li>
							<li class="clearfix">
								<img class="tovar_item_small_img" src="images/tovar/women/2.jpg" alt="" />
								<a href="product-page.html" class="tovar_item_small_title">Merino tippi sweater in geometric</a>
								<span class="tovar_item_small_price">$67.00</span>
							</li>
							<li class="clearfix">
								<img class="tovar_item_small_img" src="images/tovar/women/3.jpg" alt="" />
								<a href="product-page.html" class="tovar_item_small_title">Merino triple-stripe elbow-patch sweater</a>
								<span class="tovar_item_small_price">$94.00</span>
							</li>
							<li class="clearfix">
								<img class="tovar_item_small_img" src="images/tovar/women/4.jpg" alt="" />
								<a href="product-page.html" class="tovar_item_small_title">Collection cashmere getaway hoodie</a>
								<span class="tovar_item_small_price">$228.00</span>
							</li>
						</ul>
					</div><!-- //SIDEBAR TOVAR DETAILS -->
					
					<!-- TOVAR DETAILS WRAPPER -->
					<div class="col-lg-9 col-md-9 tovar_details_wrapper clearfix">
						<div class="tovar_details_header clearfix margbot35">
							<h3 class="pull-left"><b>Sweaters</b></h3>
							
							<div class="tovar_details_pagination pull-right">
								<a class="fa fa-angle-left" href="javascript:void(0);" ></a>
								<span>2 of 34</span>
								<a class="fa fa-angle-right" href="javascript:void(0);" ></a>
							</div>
						</div>
						
						<!-- CLEARFIX -->
						<div class="clearfix padbot40">
							<div class="tovar_view_fotos clearfix">
								<div id="slider2" class="flexslider">
									<ul class="slides">
										<li><a href="javascript:void(0);" ><img src="images/tovar/women/1.jpg" alt="" /></a></li>
										<li><a href="javascript:void(0);" ><img src="images/tovar/women/1_2.jpg" alt="" /></a></li>
										<li><a href="javascript:void(0);" ><img src="images/tovar/women/1_3.jpg" alt="" /></a></li>
										<li><a href="javascript:void(0);" ><img src="images/tovar/women/1_4.jpg" alt="" /></a></li>
									</ul>
								</div>
								<div id="carousel2" class="flexslider">
									<ul class="slides">
										<li><a href="javascript:void(0);" ><img src="images/tovar/women/1.jpg" alt="" /></a></li>
										<li><a href="javascript:void(0);" ><img src="images/tovar/women/1_2.jpg" alt="" /></a></li>
										<li><a href="javascript:void(0);" ><img src="images/tovar/women/1_3.jpg" alt="" /></a></li>
										<li><a href="javascript:void(0);" ><img src="images/tovar/women/1_4.jpg" alt="" /></a></li>
									</ul>
								</div>
							</div>
							
							<div class="tovar_view_description">
								<div class="tovar_view_title">Popover Sweatshirt in Floral Jacquard</div>
								<div class="tovar_article">88-305-676</div>
								<div class="clearfix tovar_brend_price">
									<div class="pull-left tovar_brend">Calvin Klein</div>
									<div class="pull-right tovar_view_price">$98.00</div>
								</div>
								<div class="tovar_color_select">
									<p>Select color</p>
									<a class="color1" href="javascript:void(0);" ></a>
									<a class="color2 active" href="javascript:void(0);" ></a>
									<a class="color3" href="javascript:void(0);" ></a>
									<a class="color4" href="javascript:void(0);" ></a>
								</div>
								<div class="tovar_size_select">
									<div class="clearfix">
										<p class="pull-left">Select SIZE</p>
										<span>Size & Fit</span>
									</div>
									<a class="sizeXS" href="javascript:void(0);" >XS</a>
									<a class="sizeS active" href="javascript:void(0);" >S</a>
									<a class="sizeM" href="javascript:void(0);" >M</a>
									<a class="sizeL" href="javascript:void(0);" >L</a>
									<a class="sizeXL" href="javascript:void(0);" >XL</a>
									<a class="sizeXXL" href="javascript:void(0);" >XXL</a>
									<a class="sizeXXXL" href="javascript:void(0);" >XXXL</a>
								</div>
								<div class="tovar_view_btn">
									<select class="basic">
										<?php for ($i=1; $i < 6; $i++): ?>
											<option value="<?php echo $i ?>"><?php echo $i ?></option>
										<?php endfor; ?>
									</select>
									<a class="add_bag" href="javascript:void(0);" ><i class="fa fa-shopping-cart"></i>Agregar al carrito</a>
									<a class="add_lovelist" href="javascript:void(0);" ><i class="fa fa-heart"></i></a>
								</div>
								<div class="tovar_shared clearfix">
									<p>Share item with friends</p>
									<ul>
										<li><a class="facebook" href="javascript:void(0);" ><i class="fa fa-facebook"></i></a></li>
										<li><a class="twitter" href="javascript:void(0);" ><i class="fa fa-twitter"></i></a></li>
										<li><a class="linkedin" href="javascript:void(0);" ><i class="fa fa-linkedin"></i></a></li>
										<li><a class="google-plus" href="javascript:void(0);" ><i class="fa fa-google-plus"></i></a></li>
										<li><a class="tumblr" href="javascript:void(0);" ><i class="fa fa-tumblr"></i></a></li>
									</ul>
								</div>
							</div>
						</div><!-- //CLEARFIX -->
						
						<!-- TOVAR INFORMATION -->
						<div class="tovar_information">
							<ul class="tabs clearfix">
								<li class="current">Detalle</li>
								<li>Información</li>
							</ul>
							<div class="box visible">
								<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
								<p>Curabitur pretium tincidunt lacus. Nulla gravida orci a odio. Nullam varius, turpis et commodo pharetra, est eros bibendum elit, nec luctus magna felis sollicitudin mauris. Integer in mauris eu nibh euismod gravida. Duis ac tellus et risus vulputate vehicula. Donec lobortis risus a elit. Etiam tempor. Ut ullamcorper, ligula eu tempor congue, eros est euismod turpis, id tincidunt sapien risus a quam. Maecenas fermentum consequat mi. Donec fermentum. Pellentesque malesuada nulla a mi. Duis sapien sem, aliquet nec, commodo eget, consequat quis, neque. Aliquam faucibus, elit ut dictum aliquet, felis nisl adipiscing sapien, sed malesuada diam lacus eget erat. Cras mollis scelerisque nunc. Nullam arcu. Aliquam consequat. Curabitur augue lorem, dapibus quis, laoreet et, pretium ac, nisi. Aenean magna nisl, mollis quis, molestie eu, feugiat in, orci. In hac habitasse platea dictumst. </p>
							</div>
							<div class="box">
								Original Levi 501 <br>
								Button fly<br>
								Regular fit<br>
								waist 28"-32 =16"hem<br>
								waist 33" = 17" hem<br>
								waist 34"-40"=18" hem<br>
								Levi's have started to introduce the red tab with just the (R) (registered trade mark) on the red tab<br><br>

								Size Details:<br>
								All sizes from 28-40 waist<br>
								Leg length 30", 32", 34", 36"
							</div>
							
						</div><!-- //TOVAR INFORMATION -->
					</div><!-- //TOVAR DETAILS WRAPPER -->
				</div><!-- //ROW -->
			</div><!-- //CONTAINER -->
		</section><!-- //TOVAR DETAILS -->
		
		
		<!-- BANNER SECTION -->
		<section class="banner_section">
			
			<!-- CONTAINER -->
			<div class="container">
				
				<!-- ROW -->
				<div class="row">
					
					<!-- BANNER WRAPPER -->
					<div class="banner_wrapper">
						<!-- BANNER -->
						<div class="col-lg-12 col-md-12">
							<a class="banner type4 margbot40" href="javascript:void(0);" >
								<img src="images/shabby/banner_50.png" alt="" />
							</a>
						</div><!-- //BANNER -->

					</div><!-- //BANNER WRAPPER -->
				</div><!-- //ROW -->
			</div><!-- //CONTAINER -->
		</section><!-- //BANNER SECTION -->
		
		
		<!-- NEW ARRIVALS -->
		<section class="new_arrivals padbot50">
			
			<!-- CONTAINER -->
			<div class="container">
				<h2>new arrivals</h2>
				
				<!-- JCAROUSEL -->
				<div class="jcarousel-wrapper">
					
					<!-- NAVIGATION -->
					<div class="jCarousel_pagination">
						<a href="javascript:void(0);" class="jcarousel-control-prev" ><i class="fa fa-angle-left"></i></a>
						<a href="javascript:void(0);" class="jcarousel-control-next" ><i class="fa fa-angle-right"></i></a>
					</div><!-- //NAVIGATION -->
					
					<div class="jcarousel">
						<ul>
							<li>
								<!-- TOVAR -->
								<div class="tovar_item_new">
									<div class="tovar_img">
										<img src="images/tovar/women/new/1.jpg" alt="" />
										<div class="open-project-link"><a class="open-project tovar_view" href="%21projects/women/1.html" >quick view</a></div>
									</div>
									<div class="tovar_description clearfix">
										<a class="tovar_title" href="product-page.html" >Moonglow paisley silk tee</a>
										<span class="tovar_price">$98.00</span>
									</div>
								</div><!-- //TOVAR -->
							</li>
							<li>
								<!-- TOVAR -->
								<div class="tovar_item_new">
									<div class="tovar_img">
										<img src="images/tovar/women/new/2.jpg" alt="" />
										<div class="open-project-link">
											<a class="open-project tovar_view" href="%21projects/women/1.html" >quick view</a>
										</div>
									</div>
									<div class="tovar_description clearfix">
										<a class="tovar_title" href="product-page.html" >PEASANT TOP IN SUCKERED STRIPE</a>
										<span class="tovar_price">$78.00</span>
									</div>
								</div><!-- //TOVAR -->
							</li>
							<li>
								<!-- TOVAR -->
								<div class="tovar_item_new">
									<div class="tovar_img">
										<img src="images/tovar/women/new/3.jpg" alt="" />
										<div class="open-project-link">
											<a class="open-project tovar_view" href="%21projects/women/1.html" >quick view</a>
										</div>
									</div>
									<div class="tovar_description clearfix">
										<a class="tovar_title" href="product-page.html" >EMBROIDERED BIB PEASANT TOP</a>
										<span class="tovar_price">$88.00</span>
									</div>
								</div><!-- //TOVAR -->
							</li>
							<li>
								<!-- TOVAR -->
								<div class="tovar_item_new">
									<div class="tovar_img">
										<img src="images/tovar/women/new/4.jpg" alt="" />
										<div class="open-project-link">
											<a class="open-project tovar_view" href="%21projects/women/1.html" >quick view</a>
										</div>
									</div>
									<div class="tovar_description clearfix">
										<a class="tovar_title" href="product-page.html" >SILK POCKET BLOUSE</a>
										<span class="tovar_price">$98.00</span>
									</div>
								</div><!-- //TOVAR -->
							</li>
							<li>
								<!-- TOVAR -->
								<div class="tovar_item_new">
									<div class="tovar_img">
										<img src="images/tovar/women/new/5.jpg" alt="" />
										<div class="open-project-link">
											<a class="open-project tovar_view" href="%21projects/women/1.html" >quick view</a>
										</div>
									</div>
									<div class="tovar_description clearfix">
										<a class="tovar_title" href="product-page.html" >SWISS-DOT TUXEDO SHIRT</a>
										<span class="tovar_price">$65.00</span>
									</div>
								</div><!-- //TOVAR -->
							</li>
							<li>
								<!-- TOVAR -->
								<div class="tovar_item_new">
									<div class="tovar_img">
										<img src="images/tovar/women/new/6.jpg" alt="" />
										<div class="open-project-link">
											<a class="open-project tovar_view" href="%21projects/women/1.html" >quick view</a>
										</div>
									</div>
									<div class="tovar_description clearfix">
										<a class="tovar_title" href="product-page.html" >STRETCH PERFECT SHIRT</a>
										<span class="tovar_price">$72.00</span>
									</div>
								</div><!-- //TOVAR -->
							</li>
							<li>
								<!-- TOVAR -->
								<div class="tovar_item_new">
									<div class="tovar_img">
										<img src="images/tovar/women/new/1.jpg" alt="" />
										<div class="open-project-link">
											<a class="open-project tovar_view" href="%21projects/women/1.html" >quick view</a>
										</div>
									</div>
									<div class="tovar_description clearfix">
										<a class="tovar_title" href="product-page.html" >Moonglow paisley silk tee</a>
										<span class="tovar_price">$98.00</span>
									</div>
								</div><!-- //TOVAR -->
							</li>
							<li>
								<!-- TOVAR -->
								<div class="tovar_item_new">
									<div class="tovar_img">
										<img src="images/tovar/women/new/2.jpg" alt="" />
										<div class="open-project-link">
											<a class="open-project tovar_view" href="%21projects/women/1.html" >quick view</a>
										</div>
									</div>
									<div class="tovar_description clearfix">
										<a class="tovar_title" href="product-page.html" >PEASANT TOP IN SUCKERED STRIPE</a>
										<span class="tovar_price">$78.00</span>
									</div>
								</div><!-- //TOVAR -->
							</li>
							<li>
								<!-- TOVAR -->
								<div class="tovar_item_new">
									<div class="tovar_img">
										<img src="images/tovar/women/new/3.jpg" alt="" />
										<div class="open-project-link">
											<a class="open-project tovar_view" href="%21projects/women/1.html" >quick view</a>
										</div>
									</div>
									<div class="tovar_description clearfix">
										<a class="tovar_title" href="product-page.html" >EMBROIDERED BIB PEASANT TOP</a>
										<span class="tovar_price">$88.00</span>
									</div>
								</div><!-- //TOVAR -->
							</li>
							<li>
								<!-- TOVAR -->
								<div class="tovar_item_new">
									<div class="tovar_img">
										<img src="images/tovar/women/new/4.jpg" alt="" />
										<div class="open-project-link">
											<a class="open-project tovar_view" href="%21projects/women/1.html" >quick view</a>
										</div>
									</div>
									<div class="tovar_description clearfix">
										<a class="tovar_title" href="product-page.html" >SILK POCKET BLOUSE</a>
										<span class="tovar_price">$98.00</span>
									</div>
								</div><!-- //TOVAR -->
							</li>
						</ul>
					</div>
				</div><!-- //JCAROUSEL -->
			</div><!-- //CONTAINER -->
		</section><!-- //NEW ARRIVALS -->
<?php include('footer.php'); ?>