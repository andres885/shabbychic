<?php include('header.php'); ?>
		<!-- HOME -->
		<section id="home" class="padbot0 shabby-paddingtop-100">
				<img src="images/shabby/banner1.gif" alt="">
		</section><!-- //HOME -->
		
		
		<!-- TOVAR SECTION -->
		<section class="tovar_section">
			
			<!-- CONTAINER -->
			<div class="container">
				
				<!-- ROW -->
				<div class="row">
					
					<!-- TOVAR WRAPPER -->
					<div class="tovar_wrapper" data-appear-top-offset='-100' data-animated='fadeInUp'>

						<!-- BANNER -->

						<div class="col-lg-3 col-md-12 col-xs-6 col-ss-12 shabby-container">
							<div class="row">
								<div class="col-md-6 col-lg-12">
									<ul class="shabby-categories">
										<h2>Categorías</h2>
										<?php for ($i=0; $i < 7; $i++) : ?>
										<li>
											<a href="#">Categoría <?php echo $i; ?></a>
										</li>
										<?php endfor; ?>
									</ul>
								</div>
								<div class="col-md-6  col-lg-12">
									<img src="images/shabby/banner_shabby_left.png" alt="">
								</div>
							</div>
						</div>
						
						<?php for($i=0; $i<= 8; $i++): ?>
						<!-- T OVAR1 -->
						<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-ss-12 padbot40">
							<div class="tovar_item">
								<div class="tovar_img">
									<div class="tovar_img_wrapper">
										<img class="img" src="images/tovar/women/1.jpg" alt="" />
										<img class="img_h" src="images/tovar/women/1_2.jpg" alt="" />
									</div>
									<div class="tovar_item_btns">
										<div class="open-project-link"><a class="open-project tovar_view" href="%21projects/women/1.html" >quick view</a></div>
										<a class="add_bag" href="javascript:void(0);" ><i class="fa fa-shopping-cart"></i></a>
										<a class="add_lovelist" href="javascript:void(0);" ><i class="fa fa-heart"></i></a>
									</div>
								</div>
								<div class="tovar_description clearfix">
									<a class="tovar_title" href="product-page.html" >Popover Sweatshirt in Floral Jacquard</a>
									<span class="tovar_price">$98.00</span>
								</div>
							</div>
						</div><!-- //TOVAR1 -->

					<?php endfor; ?>

					</div><!-- //TOVAR WRAPPER -->
				</div><!-- //ROW -->
			</div><!-- //CONTAINER -->
		</section><!-- //TOVAR SECTION -->
	
		
		<div class="row">
			<div class="container">
				<div class="col-lg-12 col-md-12">
					<img src="images/shabby/divider_hands.png" alt="">
				</div>
			</div>
		</div>
		
		
		<!-- SERVICES SECTION -->
		<section class="services_section">
			
			<!-- CONTAINER -->
			<div class="container">
				<h2 class="shabby-green-blur">Recuerda nuestros productos unícos</h2>
				<!-- ROW -->
				<div class="row">

					<div class="col-lg-12 col-md-12 padbot60 services_section_description" data-appear-top-offset='-100' data-animated='fadeInLeft'>

						<span>Gluten-free quinoa selfies carles, kogi gentrify retro marfa viral. Odd future photo booth flannel ethnic pug, occupy keffiyeh synth blue bottle tofu tonx iphone. Blue bottle 90′s vice trust fund gastropub gentrify retro marfa viral</span>

						<span>Gluten-free quinoa selfies carles, kogi gentrify retro marfa viral. Odd future photo booth flannel ethnic pug, occupy keffiyeh synth blue bottle tofu tonx iphone. Blue bottle 90′s vice trust fund gastropub gentrify retro marfa viral</span>

						<span>Gluten-free quinoa selfies carles, kogi gentrify retro marfa viral. Odd future photo booth flannel ethnic pug, occupy keffiyeh synth blue bottle tofu tonx iphone. Blue bottle 90′s vice trust fund gastropub gentrify retro marfa viral</span>

						<span>Gluten-free quinoa selfies carles, kogi gentrify retro marfa viral. Odd future photo booth flannel ethnic pug, occupy keffiyeh synth blue bottle tofu tonx iphone. Blue bottle 90′s vice trust fund gastropub gentrify retro marfa viral</span>

					</div>
					
				</div><!-- //ROW -->
			</div><!-- //CONTAINER -->
		</section><!-- //SERVICES SECTION -->

<?php include('footer.php'); ?>